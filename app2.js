var express = require ('express');
var nunjucks = require('nunjucks');




var app = express();

nunjucks.configure('views', {
    autoescape: true,
    express: app
});

app.get('/', function(req, res) {
    res.render('../views/index.html');
});



 var port = process.env.PORT || 1337;
 app.listen(port, function(){
 	console.log('http://127.0.0.1:' + port +'/');
});

 module.exports = app;