'use strict';
//SSL
var fs = require('fs');
var https = require('https');


var express = require ('express');
//var nunjucks = require('nunjucks');
var passport = require('passport');
var passportlocal = require('passport-local');
var passporthttp = require('passport-http');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');



var app = express();



app.set('view engine', 'ejs');
// nunjucks.configure('views', {
//     autoescape: true,
//     express: app
// });


//these must be done BEFOER passport
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(expressSession({
	secret: process.env.SESSION_SECRET || 'secret',
	resave: false,
	saveUninitialized: false
}));



app.use(passport.initialize());
app.use(passport.session());

//force api calls use a windows login form. HTTP auth.
app.use('/api', passport.authenticate('basic', {session: false}));

//local auth
passport.use(new passportlocal.Strategy(verifyCredentials));


//http auth
passport.use(new passporthttp.BasicStrategy(verifyCredentials));


function verifyCredentials(username, password, done){
	// done(null, user);
	// done(null, null);
	// done(new Error('error'));
	//checkout node cyrpto lib for password hash
	if(username === password)
	{
		done(null, {
			id: 123,
			name: username
		});
	}
	else
	{
		done(null, null);
	}
}

passport.serializeUser(function(user, done){
	done(null, user.id);
});

passport.deserializeUser(function(id, done){
	done(null, {id:id, name: id});
});


function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		next();
	}
	else
	{
		res.send(403);
	}
}

//app.use(express.static(__dirname  + '/public'));

app.get('/', function (req, res){
	res.render('../views/index', {
		isAuthenticated: req.isAuthenticated(),
		user: req.user

	});
});

app.get('/login', function (req, res){
	res.render('../views/login');
});

app.get('/logout', function (req, res){
	req.logout();
	res.redirect('/');
});



//http auth
app.get('/api/data', ensureAuthenticated, function(resq, res){
	res.json([
		{value: 'foo'},
		{ value: 'bar'},
		{value: 'baz'}
		]);
});


//local auth
app.post('/login', passport.authenticate('local'), function (req, res){
	res.redirect('/');
});

var port = process.env.PORT || 1337;
app.listen(port, function(){
 	console.log('http://127.0.0.1:' + port +'/');
});

 module.exports = app;

 //SSL SERVER
/*var server = https.createServer({
	cert: fs.readFileSync(__dirname + '/my.crt'),
	key: fs.readFileSync(__dirname + '/my.key')

}, app);*/

//app.listen(port, function(){

//SSL Server
/*server.listen(port, function(){
	console.log('http://127.0.0.1:' + port +'/');
});*/

//mac/linux cmd to generate ssl certificate
//openssl req -x509 -nodes -days 365 -newkey rsa:1024 -out my.crt -keyout my.key
 //module.exports = app;

